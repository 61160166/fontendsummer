const applicantService = {
  applicantList: [
    {
      id: 1,
      firstname: 'Mr.Sirichai',
      lastname: 'Kunatip',
      gender: 'M',
      birthday: '29-09-1999',
      nationality: 'Thai',
      tel: '091-199-9999',
      address: 'Thailand'
    },
    {
      id: 2,
      firstname: 'Miss.Kanyawat',
      lastname: 'Komchanod',
      gender: 'F',
      birthday: '19-09-1999',
      nationality: 'Thai',
      tel: '091-199-9999',
      address: 'Thailand'
    }
  ],
  lastId: 3,
  addApplicant (applicant) {
    applicant.id = this.lastId++
    this.applicantList.push(applicant)
  },
  updateApplicant (applicant) {
    const index = this.applicantList.findIndex(item => item.id === applicant.id)
    this.applicantList.splice(index, 1, applicant)
  },
  delApplicant (applicant) {
    const index = this.applicantList.findIndex(item => item.id === applicant.id)
    this.applicantList.splice(index, 1)
  },
  getApplicantNon () {
    return [...this.applicantList]
  }
}
export default applicantService
