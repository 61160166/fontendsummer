const registerService = {
  registerList: [
    {
      no: 1,
      id: '1001ac',
      name: 'Apple',
      tel: '1800-019-900',
      status: 'success'
    },
    {
      no: 2,
      id: '1002cs',
      name: 'SpaceX',
      tel: '0800-095-988',
      status: 'pending'
    }
  ],
  lastNo: 3,
  addRegister (register) {
    register.no = this.lastNo++
    this.registerList.push(register)
  },
  updateRegister (register) {
    const index = this.registerList.findIndex(item => item.no === register.no)
    this.registerList.splice(index, 1, register)
  },
  deleteRegister (register) {
    const index = this.registerList.findIndex(item => item.no === register.no)
    this.registerList.splice(index, 1)
  },
  getRegisters () {
    return [...this.registerList]
  }
}

export default registerService
